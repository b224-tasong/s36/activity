
const express = require("express");


const TaskController = require("../controllers/TaskController.js");


const router = express.Router();






router.get("/:id", (req, res) => {
	TaskController.getOneTasks(req.params.id).then((resultFromController) => res.send(resultFromController))
});





// Route for update/PUT task
router.put("/:id/update", (req, res) =>{
	TaskController.updateTask(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
});







// Use "module.exports" to export the router object to be used app.js
module.exports = router;