
let express = require("express");
let mongoose = require("mongoose");
let dotenv = require("dotenv");


const taskRoutes = require("./routes/taskRoutes.js");



const app = express();
const port = 3000;


dotenv.config();

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));



mongoose.connect(`mongodb+srv://Ricowafu:${process.env.MONGODB_PW}@224-rico.tipkxxe.mongodb.net/s36?retryWrites=true&w=majority`,
			{
				useNewUrlParser: true,
				useUnifiedTopology: true
			}
	);

let db = mongoose.connection;

db.on('error', () =>console.error('Connection Error'));

db.once('open', () =>console.log('Connected to MongoDB!'));





app.use("/tasks", taskRoutes);





app.listen(port, () => console.log(`Server is running at port ${port}`))