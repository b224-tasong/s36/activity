// Controllers contain the functions and business logic of our Express JS application
// Meaning all the operations it can do will be placed in this file



// Uses the "require" directive to allow access to the "Task" model which allows us to access Mongoose methods to perform CRUD functions
// Allows us to use the contents of the "task.js" file in the "models" folder
const Task = require("../models/Task.js")


// Controller Functions:



module.exports.getOneTasks = (taskId) => {

	return Task.findById(taskId).then((result, error) => {
		if (error) {

			console.log(error)
			return false

		}else{
			
			return result
		}	
		
	})
}




// /Function for deleting a task
module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((deletedTask, error) =>{
		if (error) {
			return false // Cant delete task or an error has occured
		}else{
			return deletedTask // "Task deleted successfully"
		}
	})
}